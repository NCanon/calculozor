package com.example.fcanon.myapplication.classes;

import com.example.fcanon.myapplication.interfaces.IOperation;

public class Division implements IOperation {
    public float operation(float fl1, float fl2) {
        if (fl2 == 0) return 0;
        return fl1 / fl2;
    }
}
