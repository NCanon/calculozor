package com.example.fcanon.myapplication;

import android.animation.ObjectAnimator;
import android.opengl.Visibility;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.system.ErrnoException;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.fcanon.myapplication.classes.Addition;
import com.example.fcanon.myapplication.classes.Division;
import com.example.fcanon.myapplication.classes.Multiplication;
import com.example.fcanon.myapplication.classes.Operation;
import com.example.fcanon.myapplication.classes.Soustraction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView TV;
    private TextView DirectResult;
    private TextView OldCalc;
    private TextView Historique;
    private TextView Erreur;
    private List<Button> ListeBoutons = new ArrayList<>();
    private Operation ope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TV = findViewById(R.id.Affichage);
        Erreur = findViewById(R.id.Erreur);
        DirectResult = findViewById(R.id.DirectResult);
        Historique = findViewById(R.id.Historique);
        OldCalc = findViewById(R.id.OldCalc);
        ListeBoutons.add((Button)findViewById(R.id.Calc0));
        ListeBoutons.add((Button)findViewById(R.id.Calc1));
        ListeBoutons.add((Button)findViewById(R.id.Calc2));
        ListeBoutons.add((Button)findViewById(R.id.Calc3));
        ListeBoutons.add((Button)findViewById(R.id.Calc4));
        ListeBoutons.add((Button)findViewById(R.id.Calc5));
        ListeBoutons.add((Button)findViewById(R.id.Calc6));
        ListeBoutons.add((Button)findViewById(R.id.Calc7));
        ListeBoutons.add((Button)findViewById(R.id.Calc8));
        ListeBoutons.add((Button)findViewById(R.id.Calc9));
        ListeBoutons.add((Button)findViewById(R.id.Plus));
        ListeBoutons.add((Button)findViewById(R.id.Minus));
        ListeBoutons.add((Button)findViewById(R.id.Divise));
        ListeBoutons.add((Button)findViewById(R.id.Multiplier));
        ListeBoutons.add((Button)findViewById(R.id.Modulo));
        ListeBoutons.add((Button)findViewById(R.id.Floating));
        ListeBoutons.add((Button)findViewById(R.id.Egale));

        for (final Button BT : ListeBoutons) {
            BT.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    RecupInfoBT(BT);
                }
            });
        }
        Button BT = findViewById(R.id.Clear);
        BT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String txt = TV.getText().toString();
                if (!txt.equals("")) {
                    TV.setText(txt.substring(0, txt.length() - 1));
                }
            }
        });
    }

    private void RecupInfoBT(Button bt) {
        String bttxt = bt.getText().toString();

        if (bttxt.equals("=")) {
            ope = new Operation(TV.getText().toString());
            if (!ope.Error) {
                Erreur.setVisibility(View.GONE);
                DirectResult.setText(ope.Resultat.toString());
                String Oldcalc = TV.getText().toString();
                OldCalc.setText(Oldcalc);

                String oldTxt = Historique.getText().toString();
                Historique.setText(TV.getText() + "=" + ope.Resultat.toString() + "\n" + oldTxt);
            }
            else {
                Erreur.setVisibility(View.VISIBLE);
            }

        }
        else {
            TV.append(bttxt);
        }
    }


}
