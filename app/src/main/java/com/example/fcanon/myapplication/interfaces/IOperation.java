package com.example.fcanon.myapplication.interfaces;

public interface IOperation {
    float operation(float fl1, float fl2);
}
