package com.example.fcanon.myapplication.classes;

import com.example.fcanon.myapplication.interfaces.IOperation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Operation {
    public String ChaineCalcul;
    public Float Resultat;
    public boolean Error = false;
    private List<Character> ListeOperateurs = Arrays.asList('/', '*', '-', '+');
    private List<String> ListeValeursWork = new ArrayList<>();

    public Operation(String chaineCalcul) {
        this.ChaineCalcul = chaineCalcul;
        ListeValeursWork.clear();
        ListeValeursWork = DecoupeChaine(chaineCalcul);
        CalculValeurs();
    }

    private void CalculValeurs()
    {
        try {
            boolean opetrouve = true;

            // Premier parcours pour résoudre les opérations obligatoires
            while (opetrouve) {
                opetrouve = false;
                for (String str : ListeValeursWork) {
                    if (str.equals("*") || str.equals("/") || str.equals("%")) {
                        opetrouve = true;
                        int index = ListeValeursWork.indexOf(str);
                        ActualiseListeValeurs(index, str);
                        break;
                    }
                }
            }

            opetrouve = true;
            while (opetrouve) {
                opetrouve = false;
                for (String str : ListeValeursWork) {
                    if (str.equals("+") || str.equals("-")) {
                        opetrouve = true;
                        int index = ListeValeursWork.indexOf(str);
                        ActualiseListeValeurs(index, str);
                        break;
                    }
                }
            }

            String res = "";
            for (String ss : ListeValeursWork) {
                res += ss;
            }
            Resultat = Float.valueOf(res);
        }
        catch (Exception e) {
            Error = true;
        }

    }

    private void ActualiseListeValeurs(int index, String signe) {
        String val1 = ListeValeursWork.get(index-1);
        String val2 = ListeValeursWork.get(index+1);
        Float fl1 = Float.valueOf(val1);
        Float fl2 = Float.valueOf(val2);
        Float res;

        switch (signe) {
            case "/":
                Division div = new Division();
                res = div.operation(fl1, fl2);
                break;
            case "*":
                Multiplication mul = new Multiplication();
                res = mul.operation(fl1, fl2);
                break;
            case "-":
                Soustraction sous = new Soustraction();
                res = sous.operation(fl1, fl2);
                break;
            case "+":
                Addition add = new Addition();
                res = add.operation(fl1, fl2);
                break;
            case "%":
                Modulo mol = new Modulo();
                res = mol.operation(fl1, fl2);
                break;
            default:
                res = 0f;
                break;
        }
        ListeValeursWork.remove(val1);
        ListeValeursWork.remove(val2);
        ListeValeursWork.remove(signe);
        ListeValeursWork.add(index-1, res.toString());
    }

    private List<String> DecoupeChaine(String chaine) {
        String ActualValue = "";
        List<String> Retour = new ArrayList<>();

        for(char ch : chaine.toCharArray()) {
            if (ListeOperateurs.contains(ch)) {
                Retour.add(ActualValue);
                Retour.add(String.valueOf(ch));
                ActualValue = "";
            }
            else {
                ActualValue += ch;
            }
        }
        if (ActualValue != "") {
            Retour.add(ActualValue);
        }
        return Retour;
    }
}
