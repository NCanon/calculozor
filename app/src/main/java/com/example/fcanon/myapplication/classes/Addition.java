package com.example.fcanon.myapplication.classes;

import com.example.fcanon.myapplication.interfaces.IOperation;

public class Addition implements IOperation {
    public float operation(float fl1, float fl2) {
        return fl1 + fl2;
    }
}
